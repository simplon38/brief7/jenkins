resource "azurerm_virtual_machine" "jenkins" {
  name                  = "jenkins-vm"
  location              = azurerm_resource_group.rg.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.main.id]
  vm_size               = "Standard_DS1_v2"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  # delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Debian"
    offer     = "debian-11"
    sku       = "11"
    version   = "latest"
  }
  storage_os_disk {
    name              = "jenkins-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "jenkins"
    admin_username = "quentin"
    custom_data = data.template_cloudinit_config.config_jenkins.rendered
  }

  os_profile_linux_config {
    ssh_keys {
      key_data = file("~/.ssh/id_rsa.pub")
      path = "/home/quentin/.ssh/authorized_keys"
    }
    disable_password_authentication = true
  }
  tags = {
    environment = "staging"
  }
}
